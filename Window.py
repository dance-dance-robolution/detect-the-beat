from pygame import mixer
from tkinter import * 
from PIL import Image, ImageTk
from random import *
import math
import threading
import imutils
import cv2

# Enum for indexing into meta data of songs
class Song():
	FILE_NAME = 0
	NAME = 1
	ARTIST = 2
	BPM = 3

def init_dict_songs():
	'''
	Initializes a dictionary of songs where key=bpm, and
	value=path/to/music/file
	'''
	song_dic = {
		60 : ['music2/60bpm.mp3', 'The Lion Sleep Tonight', 'The Tokens'],
		70 : ['music2/70bpm.mp3', 'Bohemian Rhapsody', 'Queen'],
		80 : ['music2/80bpm.mp3', 'Halo', 'Beyonce'],
		90 : ['music2/90bpm.mp3', 'Replay', 'Iyaz'],
		100 : ['music2/100bpm.mp3', 'Dancing Queen', 'ABBA'],
		110 : ['music2/110bpm.mp3', 'Eye of the Tiger', 'Survivor'],
		120 : ['music2/120bpm.mp3', 'Call Me Maybe', 'Carly Rae Jepsen'],
		130: ['music2/130bpm.mp3', 'Moves Like Jagger', 'Maroon 5'], 
		140: ['music2/140bpm.mp3', 'Every Time We Touch', 'Cascada'] 
	}
	return song_dic

class Window(Frame):
# Defines visual window, also in charge of changing music

	def __init__(self, master=None):
		# parameters that you want to send through the Frame class. 
		Frame.__init__(self, master)   

		#reference to the master widget, which is the tk window                 
		self.master = master

		# initialize songs
		self.dict_songs = init_dict_songs()

		# intialize current bpm
		self.current_bpm = 120

		mixer.init()

		#with that, we want to then run init_window, which doesn't yet exist
		self.init_window()

    #Creation of init_window
	def init_window(self):
		# changing the title of our master widget      
		self.master.title("Dance Dance Robolution!")

		# allowing the widget to take the full space of the root window
		self.pack(fill=BOTH, expand=1)

		# load background image
		load = Image.open("bg.jpg")
		render = ImageTk.PhotoImage(load)
		img = Label(self, image=render)
		img.image = render
		img.place(x=0, y=0)
		
		# place buttons
		play_button = Button(self, text="Play Random Song",command=self.play_rand_song)
		play_button.place(x=100, y=0)

		stop_button = Button(self, text="Stop Music",command=self.stop_song)
		stop_button.place(x=0, y=0)

		# create labels for bpm
		self.song_title = Label(self, text="", font=("Helvetica", 30))
		self.song_title.place(x=0, y=500)

		# create labels for bpm
		self.song_bpm_label = Label(self, text="SONG BPM", font=("Helvetica", 30))
		self.song_bpm_label.place(x=700, y=300)

	# function for testing purposesS
	def play_rand_song(self):
		bpm = randint(60, 180)
		# print(bpm)
		self.play_song_with_bpm(bpm)

	def play_song_with_bpm(self, bpm):
		'''
		bpm: integer specifying the bpm
		plays mp3 that is closest to teh given bpm
		'''

		if bpm == None or bpm < 0:
			return

		# only switch if the bpm is different than the song that is currently playing 
		if bpm != self.current_bpm:
			if bpm in self.dict_songs:
				mixer.music.fadeout(100)
				mixer.music.load(self.dict_songs.get(bpm)[Song.FILE_NAME])
				mixer.music.play(start=25)
			elif bpm / 2 in self.dict_songs: # dancing too fast, take the slower version
				bpm = bpm / 2 
				mixer.music.fadeout(100)
				mixer.music.load(self.dict_songs.get(bpm)[Song.FILE_NAME])
				mixer.music.play(start=25)
			else:
				return 
		self.update_song_title(self.dict_songs.get(bpm))
		self.update_song_bpm(bpm)
		self.current_bpm = bpm

	def update_song_title(self, song_data):
		'''
		Updates song title, given bpm
		'''
		text = "\"" + song_data[Song.NAME] + "\" by " + song_data[Song.ARTIST]
		self.song_title.config(text=text)

	def update_song_bpm(self, bpm):
		text = "SONG BPM: \n" + str(bpm)
		self.song_bpm_label.config(text=text)

	def stop_song(self): 
		'''
		Stops music if currently playing 
		'''
		mixer.music.stop()