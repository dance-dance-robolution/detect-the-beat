# CPSC 659 Fall '18 Final Project: Detect the Beat

Collaborators: Julia Lu, Roland Huang, Valerie Chen

## Technical Summary

This package has the following Python dependencies (using pip install):
- opencv-python
- imutils
- pyaudio
- pygame

This system is written for Python 3.

## Summary

This system will use the system camera (webcam) to track a yellow ball or glove, held or worn by the user who is dancing to a beat. Based on the detected beat, music will be played back.

## Modes

### Playing a variety of songs

By default, the system will play songs from the `music/` folder upon detection of a beat. For copyright reasons, we have not included the files we used: recommendations are below. Please add MP3 files in the `music/` folder varying between 60bpm and 140bpm (incremented by 10bpm, for a total of 9 files).

- `60bpm.mp3`: "The Lion Sleeps Tonight", The Tokens
- `70bpm.mp3`: "Bohemian Rhapsody", Queen
- `80bpm.mp3`: "Halo", Beyoncé
- `90bpm.mp3`: "Replay", Iyaz
- `100bpm.mp3`: "Dancing Queen", ABBA
- `110bpm.mp3`: "Eye of the Tiger", Survivor
- `120bpm.mp3`: "Call Me Maybe", Carly Rae Jepsen
- `130bpm.mp3`: "Moves Like Jagger", Maroon 5 & Christina Aguilera
- `140bpm.mp3`: "Everytime We Touch", Cascada

Run `./webcam.py` to test this system. Use the `--no-kalman` flag to turn off the Kalman filter used for tracking the ball.

### Playing generated music

Run `./webcam.py --music-gen` to test this system. If you would like to control the pitch of the generated music, acquire a red-colored signaller (e.g. a red ball or glove). Run the system with the `--multi-tracking` flag and move the red signaller vertically to control the pitch range of the generated music.

## Prior Work

1. Marynel Vazquez
	- https://gitlab.com/cpsc659-bim/assignments/f18-assignments/
2. Davy Wybiral
	- http://davywybiral.blogspot.com/2010/09/procedural-music-with-pyaudio-and-numpy.html
3. Adrian Rosebrock
	- https://www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv/

