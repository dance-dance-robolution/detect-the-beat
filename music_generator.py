#!/usr/bin/env python

'''
This code is a modification of a procedural music tutorial from Davy Wybiral (2010):
http://davywybiral.blogspot.com/2010/09/procedural-music-with-pyaudio-and-numpy.html
'''

import math
import numpy as np
import pyaudio
import itertools
from scipy import interpolate
from operator import itemgetter
import sys
from random import randint, getrandbits

class Note:

    NOTES = ['a', 'a#', 'b', 'c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#']

    def __init__(self, note, octave=4):
        self.octave = octave
        if isinstance(note, int):
            self.index = note
            self.note = Note.NOTES[note]
        elif isinstance(note, str):
            self.note = note.strip().lower()
            self.index = Note.NOTES.index(self.note)

    def transpose(self, halfsteps):
        octave_delta, note = divmod(self.index + halfsteps, 12)
        return Note(note, self.octave + octave_delta)

    def frequency(self):
        # freq(A1) = 55.00 Hz
        halfsteps = (self.octave - 1)*12 + self.index
        return 55 * (2.0 ** (halfsteps / 12.0))

    def __float__(self):            # ?? a cosa serve?
        return self.frequency()


class Scale:

    def __init__(self, root, intervals):
        self.root = Note(root.index, root.octave)
        self.intervals = intervals

    def get(self, index):
        intervals = self.intervals
        if index < 0:
            index = abs(index)
            intervals = reversed(self.intervals)
        intervals = itertools.cycle(intervals)
        note = self.root
        for i in range(index):
            note = note.transpose(next(intervals))
        return note

    def index(self, note):
        intervals = itertools.cycle(self.intervals)
        index = 0
        x = self.root
        while x.octave != note.octave or x.note != note.note:
            x = x.transpose(next(intervals))
            index += 1
        return index

    def transpose(self, note, interval):
        return self.get(self.index(note) + interval)


def sine(frequency, length, rate):
    length = int(length * rate)
    factor = float(frequency) * (math.pi * 2) / rate
    return np.sin(np.arange(length) * factor)


def shape(data, points, kind='slinear'):
    items = points.items()
    sorted(items, key=itemgetter(0))
    keys = list(map(itemgetter(0), items))
    vals = list(map(itemgetter(1), items))
    interp = interpolate.interp1d(keys, vals, kind=kind)
    factor = 1.0 / len(data)
    shape = interp(np.arange(len(data)) * factor)
    return data * shape

base_volume = 5

def harmonics1(freq, length):
    a = sine(freq * 1.00, length, 44100)
    b = 0
    c = 0
    return (a + b + c) * base_volume

def harmonics2(freq, length):
    a = sine(freq * 1.00, length, 44100)
    b = sine(freq * 2.00, length, 44100) * 0.5
    return (a + b) * base_volume

# base_length = 1 # defaults to half a second aka 120 bpm

def pluck1(note, value):
    chunk = harmonics1(note.frequency(), value)
    return shape(chunk, {0.0: 0.0, 0.005: 1.0, 0.25: 0.5, 0.9: 0.1, 1.0: 0.0})

def pluck2(note, value):
    chunk = harmonics2(note.frequency(), value)
    return shape(chunk, {0.0: 0.0, 0.5: 0.75, 0.8: 0.4, 1.0: 0.1})


def triad(n, scale, value):
    root = scale.get(n)
    third = scale.transpose(root, 2)
    fifth = scale.transpose(root, 4)
    return pluck1(root, value) + pluck1(third, value) + pluck1(fifth, value)

class MusicGenerator():
    def __init__(self):
        self.root = Note('A', 2)
        self.scale = Scale(self.root, [2, 2, 1, 2, 2, 2, 1]) # major
        # self.scale = Scale(self.root, [2, 2, 3, 2, 3]) # pentatonic
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(format=pyaudio.paFloat32, channels=1, rate=44100, output=1)

    def playChord(self, n):
        self.stream.write(triad(n, self.scale, 1).astype(np.float32).tostring())

    def randomBeat(self, duration, root):
        self.stream.write(triad(root + randint(0, 2), self.scale, 0.25 * duration).astype(np.float32).tostring())
        self.stream.write(pluck1(self.scale.get(root + randint(0,6)), 0.25 * duration).astype(np.float32).tostring())
        self.stream.write(pluck1(self.scale.get(root + randint(0,6)), 0.25 * duration).astype(np.float32).tostring())
        self.stream.write(pluck1(self.scale.get(root + randint(0,6)), 0.25 * duration).astype(np.float32).tostring())

    def playMeasure(self, speed):
        self.stream.write(triad(0, self.scale, 0.25 * speed).astype(np.float32).tostring())
        self.stream.write(pluck1(self.scale.get(0), 0.25 * speed).astype(np.float32).tostring())
        self.stream.write(pluck1(self.scale.get(2), 0.25 * speed).astype(np.float32).tostring())
        self.stream.write(pluck1(self.scale.get(4), 0.25 * speed).astype(np.float32).tostring())
        self.stream.write(triad(2, self.scale, 0.25 * speed).astype(np.float32).tostring())
        self.stream.write(pluck1(self.scale.get(4), 0.25 * speed).astype(np.float32).tostring())
        self.stream.write(pluck1(self.scale.get(2), 0.25 * speed).astype(np.float32).tostring())
        self.stream.write(pluck1(self.scale.get(0), 0.25 * speed).astype(np.float32).tostring())

    def close(self):
        stream.close()
        p.terminate()
