#!/usr/bin/env python

'''
This code is a modification of coursework from CPSC 659 (Building Interactive Machines),
taught at Yale University under Professor Marynel Vazquez in Fall 2018
'''

import copy
import cv2
import numpy as np
import time

from numpy.linalg import inv
from numpy import matmul, add, subtract, transpose

def KF_predict_step(mu, Sigma, A, R):
    pred_mu = matmul(A, mu)    
    pred_Sigma = add(matmul(matmul(A, Sigma), transpose(A)), R)
    return pred_mu, pred_Sigma

def KF_measurement_update_step(pred_mu, pred_Sigma, z, C, Q):
    inverse_term = inv(add(matmul(matmul(C, pred_Sigma), transpose(C)), Q))
    K = matmul(matmul(pred_Sigma, transpose(C)), inverse_term) # Kalman gain
    corrected_mu = add(pred_mu, matmul(K, subtract(z, matmul(C, pred_mu))))
    
    I = np.identity(len(K))
    corrected_Sigma = matmul(subtract(I, matmul(K, C)), pred_Sigma)

    return corrected_mu, corrected_Sigma

class KalmanFilter():
	def __init__(self):
		"""
        Constructor
        """

        # Filter variables
		self.mu = None                                     # state mean
		self.Sigma = None                                  # state covariance
		self.R = None                                      # covariance for the process model
		self.Q = None                                      # covariance for the measurement model
		self.A = None                                      # matrix that predicts the new state based on the prior state
		self.C = None                                      # matrix that transforms states into observations

        # Initialize constant filter values
		self.initialize_process_covariance()
		self.initialize_measurement_covariance()
		self.assemble_C_matrix()
                
        # other
		self.last_time = None                                   # Last time-stamp of when a filter update was done

	def next_prediction(self, x, y):
		if self.mu is None and x is not None and y is not None:
			self.initialize_mu_and_sigma(x, y)
			self.last_time = time.time()
			return None

		if self.last_time is None:
			return None

		current_time = time.time()
		delta_t = current_time - self.last_time

        # assemble A matrix: helps generate new state from prior state and elapsed time
		self.assemble_A_matrix(delta_t)

        # prediction step: predict new mean and covariance
		self.mu, self.Sigma = KF_predict_step(self.mu, self.Sigma, self.A, self.R)

		self.last_time = current_time

        # measurement update step: correct mean and covariance
		if x is not None and y is not None:
			z = np.array([[x], [y]])
			self.mu, self.Sigma = KF_measurement_update_step(self.mu, self.Sigma, z, self.C, self.Q)

		# note: return x and y represent pixel locations which are ints, but numbers produced
		# in matmul process are floats, thus we cast below
		return int(self.mu[0, 0]), int(self.mu[1, 0])

	def assemble_A_matrix(self, delta_t):
        # param delta_t: elapsed time (in seconds) since last prediction
		self.A = np.array([[1, 0, delta_t, 0, 0.5*delta_t*delta_t, 0],
	                       [0, 1, 0, delta_t, 0, 0.5*delta_t*delta_t],
	                       [0, 0, 1, 0, delta_t, 0],
	                       [0, 0, 0, 1, 0, delta_t],
	                       [0, 0, 0, 0, 1, 0],
	                       [0, 0, 0, 0, 0, 1]])

	def assemble_C_matrix(self):
		# projection to just the position
		self.C = np.array([[1, 0, 0, 0, 0, 0],
	                       [0, 1, 0, 0, 0, 0]])

	def initialize_process_covariance(self):
		self.R = np.array([[8, 0, 2, 0, 1, 0],
	                       [0, 8, 0, 2, 0, 1],
	                       [2, 0, 8, 0, 2, 0],
	                       [0, 2, 0, 8, 0, 2],
	                       [1, 0, 2, 0, 8, 0],
	                       [0, 1, 0, 2, 0, 8]]) * 10 # NOTE THE MULTIPLIER!

	def initialize_measurement_covariance(self):
		self.Q = np.identity(2) * 400 # NOTE THE MULTIPLIER!

	def initialize_mu_and_sigma(self, x, y):
		self.mu = np.array([[x], [y], [5], [5], [1], [1]])
		self.Sigma = np.identity(6) * 400 # NOTE THE MULTIPLIER!
