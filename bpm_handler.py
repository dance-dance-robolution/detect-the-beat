#!/usr/bin/env python

import numpy as np
from collections import deque

HISTORY_SIZE = 12
MAX_BPM_FLUCTUATION = 10

times = deque(maxlen=HISTORY_SIZE)
instant_bpms = deque(maxlen=HISTORY_SIZE)
curr_bpm = None

def round_to_nearest_ten(n):
	low = int(n / 10) * 10
	high = low + 10
	if n - low < 5:
		return low
	return high

def getMostRecent(n):
	mostRecent = []
	for i in range(n):
		mostRecent.append(instant_bpms[i])
	return mostRecent

def update_bpm():
    if len(times) >= 2 and times[0] is not None and times[1] is not None:
    	instant_bpms.appendleft(60 / (times[0]-times[1]))

    if len(instant_bpms) >= 4:
    	mostRecent = getMostRecent(4)
    	minBpm = min(mostRecent)
    	maxBpm = max(mostRecent)
    	if maxBpm - minBpm < MAX_BPM_FLUCTUATION:
    		curr_bpm = round_to_nearest_ten(np.mean(mostRecent))
    		print("BPM: {}".format(curr_bpm))
    		return curr_bpm
    	elif minBpm > 50 and maxBpm < 150:
    		print("highly variant bpm")

def beat(ts):
	times.appendleft(ts)
	return update_bpm()
